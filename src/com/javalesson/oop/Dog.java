package com.javalesson.oop;

public class Dog {

    //    public static dogsCount;
    private int paws = 4;
    private int tail = 1;
    private String name;
    private String bread;
    private String size;

    public void setName(String dogsName) {
        this.name = dogsName;
    }

    public String getName() {
        return name;
    }

    public int getTail() {
        return tail;
    }

    public void setTail(int tail) {
        if (tail == 1) {
            this.tail = tail;
        } else {
            System.out.println("Была попытка создать экземпляр класса Dog с " + tail + " хвостами");
        }
    }

    public String getBread() {
        return bread;
    }

    public void setBread(String bread) {
        this.bread = bread;
    }

    public int getPaws() {
        return paws;
    }

    public void setPaws(int paws) {
        if (paws == 4) {
            this.paws = paws;
        } else {
            System.out.println("Была попытка создать экземпляр класса Dog с " + paws + " лапами");
        }

    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        if (size.equalsIgnoreCase("big") || size.equalsIgnoreCase("average") || size.equalsIgnoreCase("small")) {
            this.size = size;
        } else {
            System.out.println("Указан неверный размер собаки");
        }
    }

    public void bark() {
        if (size.equalsIgnoreCase("big")) {
            System.out.println("wof-wof");
        }
        if (size.equalsIgnoreCase("average")) {
            System.out.println("raf-raf");
        }
        if (size.equalsIgnoreCase("small")) {
                System.out.println("tiaf-tiaf");
            }
        }

    public void bite() {

    }
}


