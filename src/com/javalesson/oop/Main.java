package com.javalesson.oop;

public class Main {
    public static void main(String[] args) {
        Dog dvor = new Dog();
        dvor.setPaws(4);
        dvor.setTail(2);
        dvor.setName("Valik");
        dvor.setBread("Dvor terrier");
        dvor.setSize("average");
        dvor.bark();


        Dog sheppard = new Dog();
        sheppard.setPaws(5);
        sheppard.setTail(1);
        sheppard.setName("Lapa");
        sheppard.setBread("Sheppard");
        sheppard.setSize("big");
        sheppard.bark();



        System.out.println("Dvor terrier's name is "+dvor.getName());
        System.out.println("Sheppard's name is "+sheppard.getName());
        System.out.println("To "+dvor.getName()+" set "+ dvor.getTail()+" tail");
        System.out.println("To "+ sheppard.getName()+" set "+ sheppard.getPaws()+" paws");

    }
}
